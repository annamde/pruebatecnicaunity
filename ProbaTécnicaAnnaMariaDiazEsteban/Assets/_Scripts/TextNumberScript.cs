﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextNumberScript : MonoBehaviour
{
    [Header("Animation")]
    public Animation myAnim;
    public AnimationClip entryAnim;
    public AnimationClip exitAnim;

    [Header("Time")]
    public int waitTime = 2;

    bool exit = true;
    float counter = 0.0f;

    void Start()
    {
        exit = true;
    }

    public void TextNumberAnimation()
    {
        if (exit)
        {
            PlayAnimation(entryAnim);
            exit = false;
        }
        
        if (!exit)
        {
            counter += Time.deltaTime;
        }

        if (counter >= waitTime)
        {
            counter = 0.0f;
            GameManager.SetAnimationExitText();
            exit = true;
        }
    }

    public void ExitTextNumber()
    {
        PlayAnimation(exitAnim);
        
        GameManager.SetAnimationNumbers();
    }

    public void PlayAnimation(AnimationClip anim)
    {
        myAnim.Play(anim.name);
    }
}
