﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { set; get; }

    static bool created = false;

    public static bool correct;
    public static bool wrongAnswer;
    public static bool lastChance;

    [Header("Random Positions")]
    public List<GameObject> positions;

    static float finishCounter = 0;

    [Header("Reset Time")]
    public float resetTime = 2;

    [Header("Text")]
    public Text encertsText;
    public Text erradestext;

    public static int encertsCounter;
    public static int erradesCounter;

    public enum GameState
    {
       ASSIGNVALES=0,
       ANIMATIONMAINTEXT,
       ANIMATIONEXITTEXT,
       ANIMATIONNUMBERS,
       PLAY,
       RETURN
    }

    public static GameState currentState;

    [Header("Scripts")]
    public TextManager myTM;
    public TextNumberScript myTextNumber;
    public NumberScript correcteNumber1;
    public List<NumberScript> numbersScripts = new List<NumberScript>();

    List<int> randomPositions = new List<int>();

    // Start is called before the first frame update
    private void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(gameObject);
            created = true;
            Instance = this;
        }

        else
        {
            Destroy(this.gameObject);
        }

        erradesCounter = 0;
        encertsCounter = 0;

        SetAssignValues();
    }


    // Update is called once per frame
    void Update()
    {
        switch(currentState)
        {
            case GameState.ASSIGNVALES:
                UpdateAssignValues();
                break;
            case GameState.ANIMATIONMAINTEXT:
                UpdateAnimationMainText();
                break;
            case GameState.ANIMATIONEXITTEXT:
                UpdateAnimationExitText();
                break;
            case GameState.ANIMATIONNUMBERS:
                UpdateAnimationNumbers();
                break;
            case GameState.PLAY:
                UpdatePlay();
                break;
            case GameState.RETURN:
                UpdateReturn();
                break;
        }

        SetText(encertsText, encertsCounter);
        SetText(erradestext,erradesCounter);
    }

    //---------- ASSIGNVALES----------
    public static void SetAssignValues()
    {
        correct = false;
        wrongAnswer = false;
        lastChance = false;
        finishCounter = 0;
        currentState = GameState.ASSIGNVALES;
    }
    
    void UpdateAssignValues()
    {
        myTM.NewNumber();
        
    }

    //--------ANIMATIONMAINTEXT---------
    public static void SetAnimationMainText()
    {
        correct = false;
        finishCounter = 0;

        currentState = GameState.ANIMATIONMAINTEXT;
    }

    void UpdateAnimationMainText()
    {
        myTextNumber.TextNumberAnimation();
    }

    //----------ANIMATIONEXITTEXT----------
    public static void SetAnimationExitText()
    {
        correct = false;
        finishCounter = 0;

        currentState = GameState.ANIMATIONEXITTEXT;
    }

    public void UpdateAnimationExitText()
    {
        myTextNumber.ExitTextNumber();
    }

    //----------ANIMATIONNUMBERS---------
    public static void SetAnimationNumbers()
    {
        correct = false;
        finishCounter = 0;
        GameManager.Instance.SetNumberPositions();

        currentState = GameState.ANIMATIONNUMBERS;
    }

    void UpdateAnimationNumbers()
    {
        if (!myTextNumber.myAnim.isPlaying)
        {
            correcteNumber1.PlayFadeIn();
            for (int i = 0; i < numbersScripts.Count; i++)
            {
                numbersScripts[i].PlayFadeIn();
            }
        }
    }

    //----------PLAY------------
    public static void SetPlay()
    {
        correct = false;
        finishCounter = 0;

        currentState = GameState.PLAY;
    }

    void UpdatePlay()
    {
        if(correct)
        {
            encertsCounter++;

            for (int i = 0; i < numbersScripts.Count; i++)
            {
                numbersScripts[i].IsInteractable();
            }
            correcteNumber1.PlayCorrectAnim();

            SetReturn();

        }
        else if (lastChance)
        {
            correcteNumber1.PlayCorrectAnim();
            SetReturn();
        }
    }

    //----------RETURN-----------------
    public static void SetReturn()
    {
        finishCounter = 0;

        currentState = GameState.RETURN;
    }

    void UpdateReturn()
    {
        finishCounter += Time.deltaTime;
        if(finishCounter >= resetTime)
        {
            SetAssignValues();
        }
    }

    //----------------------------------------
    public void SetNumberPositions()
    {
        randomPositions.Clear();
        int temp = Random.Range(0, positions.Count);
        correcteNumber1.RandomPosition(positions[temp].transform);

        randomPositions.Add(temp);
        for (int i = 0; i < numbersScripts.Count; i++)
        {
             temp = Random.Range(0, positions.Count);
            while(randomPositions.Contains(temp))
            {
                temp = Random.Range(0, positions.Count);
            }
            randomPositions.Add(temp);
            numbersScripts[i].RandomPosition(positions[temp].transform);
        }
    }

    public void SetText(Text temp, int num)
    {
        temp.text = num.ToString();
    }

}
