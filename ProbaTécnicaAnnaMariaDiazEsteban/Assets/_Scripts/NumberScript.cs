﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberScript : MonoBehaviour
{
    [Header("Animation")]
    public Animation myAnim;
    public AnimationClip fadeIn;
    public AnimationClip fadeOut;
    public AnimationClip correctAnim;
    public AnimationClip wrongAnim;

    [Header("Button")]
    public Button myButton;

    public void PlayFadeIn()
    {
        PlayAnimation(fadeIn);
        GameManager.SetPlay();
    }

    public void PlayFadeOut()
    {
        PlayAnimation(fadeOut);
    }

    public void PlayAnimation(AnimationClip anim)
    {
        myAnim.Play(anim.name);
    }

    public void PlayCorrectAnim()
    {
        myAnim.Play(correctAnim.name);
    }
    public void PlayWrongAnim()
    {
        PlayAnimation(wrongAnim);
    }

    public void FalseAnswer()
    {
        GameManager.erradesCounter++;
        if (GameManager.wrongAnswer)
        {
            GameManager.lastChance = true;
            PlayWrongAnim();
        }
        else if (!GameManager.wrongAnswer)
        {
            GameManager.wrongAnswer = true;
            PlayWrongAnim();
        }
    }

    public void RandomPosition(Transform trans)
    {
        this.gameObject.transform.position = trans.position;
    }

    public void CorrectAnswer()
    {
        GameManager.correct = true;
    }
    
    public void IsInteractable()
    {
        if(myButton.IsInteractable())
        {
            PlayWrongAnim();
        }
    }
}
