﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{
    [Header("Doc")]
    public string mainTextNumberDoc;
    public string numbersDoc;
    
    TextAsset mainTextNumber, correctNumbers, otherNumbers;
    string[] mainTextNumberList, correctNumberList, otherNumberList;

    [Header("Text")]
    public Text mainText;
    public Text numberText1;
    public Text[] numbersTexts;

    List<int> addNumbers = new List<int>();

    // Start is called before the first frame update
    void Start()
    {
        mainTextNumber = Resources.Load<TextAsset>(mainTextNumberDoc);
        correctNumbers = Resources.Load<TextAsset>(numbersDoc);
        otherNumbers = Resources.Load<TextAsset>(numbersDoc);

        if (mainTextNumber != null)
        {
            mainTextNumberList = mainTextNumber.text.Split('\n');
        }
       
        if(correctNumbers != null && otherNumbers != null)
        {
            correctNumberList = correctNumbers.text.Split('\n');
            otherNumberList = otherNumbers.text.Split('\n');
        }
    }
    
    public void NewNumber()
    {
        addNumbers.Clear();
        int random = Random.Range(0, mainTextNumberList.Length);

        mainText.text = mainTextNumberList[random];
        numberText1.text = correctNumberList[random];
      
        addNumbers.Add(random);
        for (int i = 0; i < numbersTexts.Length; i++)
        {
            int temp = Random.Range(0, otherNumberList.Length);
           
           while(addNumbers.Contains(temp))
           {
               temp = Random.Range(0, otherNumberList.Length);
           }

            addNumbers.Add(temp);
            numbersTexts[i].text = otherNumberList[temp];
        }

        GameManager.SetAnimationMainText();
    }
}
